import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  //user:
  // todo: show fighter info (image, name, health, etc.)
  if(typeof fighter === "undefined"){
    return;
  }
  const arrEls = [
    /*name*/   createFighterText (fighter.name, "center"),
    /*img*/    createFighterImage(fighter),
    /*health*/ createFighterText (fighter.health),
    /*attack*/ createFighterText (fighter.attack),
    /*defense*/createFighterText (fighter.defense)
  ];

  fighterElement.appendChild(...arrEls);
  return fighterElement;
}

export function createFighterText(val, align="left") {
  const attributes = { 
    style: `text-align:${align}margin-bottom:2px;` 
  };
  const textEl = createElement({
    tagName: "div", className: "", attributes
  })
  textEl.innerText = ""+val;

  return textEl;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
